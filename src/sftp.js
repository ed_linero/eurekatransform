const fs = require('fs');
_ = require('lodash');

function initSFTPConnection(sftp, config, process, localPath, remotePath, callBack){

	return sftp.session(config)

	.then((sftpSession) => {

		sftp.ls(remotePath)
			.then((directory) => {

				if(process === 'extract'){
					var filteredList = _.filter(directory.entries, function(entry) {

						return entry.filename.startsWith('Amplify') && entry.filename.endsWith('.csv')
					})
	             
					return Promise.all(filteredList.map(file => {

							return sftp.get(remotePath + file.filename, localPath + file.filename)
			
					}))
				}else{

					var filteredList = _.filter(fs.readdirSync(localPath), function(entry) {

						return entry.endsWith('.csv')
					})

					return Promise.all(filteredList.map(file => {

					  return sftp.put(localPath + file, remotePath + file)
					
					}))
				}				
			})
	        .then(results => {
	    		if(!(results.includes(false))){
	    			sftpSession.end();
	    			callBack();
	    		}
	    		else{
	    			sftpSession.end();
	    		}
		    })
	      	.catch((err) => {
				console.log(err, 'catch error');
				sftpSession.end();
			});
      })
	  .catch((err) => {
				console.log(err, 'catch error');
				sftpSession.end();
	  });
}

module.exports = initSFTPConnection;


