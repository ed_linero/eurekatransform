var combine = require('./combine');
var fs = require('fs');

function runTransform(path, array, combinationFile, runFilter){
  fs.readdir(path, (err, files) => {
    files.forEach(file => {

      if(file.startsWith('Amplify') && file.endsWith('.csv')){
    	array.push(path + file);
      }
    });
    combine(array, path + combinationFile, runFilter);
  })
}

module.exports = runTransform;

