var fs = require('fs');
var stream = require('stream');
_ = require('lodash');



function runFilter(file, headerRow, filter, runSplit){

  var rows = setupFile(file, headerRow);

  var filteredRows = applyFilter(rows, filter);
  
  writeToFile(file, filteredRows, runSplit);

}

function applyFilter(contents, filter){
   
   //if filter is empty, skip function.
   if(!(_.isEmpty(filter))){

     var prop = null;
     var rowsToDelete = [];

     convertHeaderRow(contents, filter, prop);
     
     findFilterMatches(contents, filter, rowsToDelete, prop);
    
     removeFilteredEntries(contents, rowsToDelete);

   }
   return cleanArray(contents);
}


function removeFilteredEntries(contents, rowsToDelete){
  for(var m = 0; m < rowsToDelete.length; m++){
       contents[rowsToDelete[m]] = "";
     }
}

function convertHeaderRow(array, filter, property){
  //converts header row to indexes for searching
  var headerRow = array[0].split(',');
     for(var i = 0; i < headerRow.length; i++){
       for(property in filter){
         if(headerRow[i] === property){
          filter[i] = filter[property];
          delete filter[property];
         }
       }
     }

}

function findFilterMatches(contents, filter, rowsToDelete, property){

  for(var j = 1; j < contents.length; j++){
       var row = contents[j].split(',');
       //looping through columns in row
       for(var col = 0; col < row.length; col++){
        for(property in filter){
          //column of filter matches
          if(col === Number(property)){
          //filter matches the contents of the column in the row
            var formattedEntry = row[col].replace(/(\r\n|\r)/gm,"");
            var filterValues = filter[property];
            var match = false;
            for(var k = 0; k < filterValues.length; k++){
              if(filterValues[k].toString() === formattedEntry) {
              //add to delete array to remove the row
              match = true; 
            }
          }
            if(!match){rowsToDelete.push(j);}
        }
      }
    }
  }
}

//returns content to put into file
function setupFile(file, headerRow){
  //split up contents of combined file and manipulate the rows
  var contents = fs.readFileSync(file, 'utf-8');
  var rows = cleanArray(contents.split('\n'));


  if(rows[0] != headerRow){
    //add header row
    rows.unshift(headerRow);
  }

  return rows;
}

function writeToFile(file, contents, runSplit){
  var filteredArray = [];

  var stream = fs.createWriteStream(file);
    stream.once('open', function(fd) {
    contents.forEach(row => {
     stream.write(row);
     stream.write('\n');
     filteredArray.push(row);
     if(row === contents[contents.length - 1]) runSplit(filteredArray);
    });
    stream.end();
  });
}

function cleanArray(actual) {
  var newArray = [];
  for (var i = 0; i < actual.length; i++) {
    if (actual[i]) {
      newArray.push(actual[i]);
    }
  }
  return newArray;
}

module.exports = runFilter;
