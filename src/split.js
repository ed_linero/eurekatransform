var fs = require('fs');
var stream = require('stream');
_ = require('lodash');

function runSplit(filteredArray, columns, outputFileHeader, outputFileName){

  var multiInputArray = convertToMultiArray(filteredArray);

  var outPutArray = setupFile(outputFileName, outputFileHeader);

  var multiOutputArray = convertToMultiArray(outPutArray);

  applySplit(multiInputArray, multiOutputArray, columns, outputFileName);
  
  writeToFile(outputFileName, multiOutputArray);
}


function applySplit(srcArray, destArray, columns, outputFileName){

  for(var i = 0; i < columns.length; i++){
    var srcIndex = findIndex(srcArray, columns[i], false, outputFileName);
    var destIndex = findIndex(destArray, columns[i], true, outputFileName);
    
    for(var j = 1; j < srcArray.length; j++){

      if(destArray[j]){
        destArray[j][destIndex] = srcArray[j][srcIndex];
      }else{
        var dummyRow = makeDummyRow(destArray);
        destArray.push(dummyRow);
        destArray[j][destIndex] = srcArray[j][srcIndex];
      }
    }
  }
}

function makeDummyRow(multiArray){
  var dummyRow = [];
  for(var k = 0; k < multiArray[0].length; k++){
    dummyRow.push('');
  }
  return dummyRow;
}


function fileExists(file){
  return fs.existsSync(file);
}

function createFile(file){

  fs.writeFileSync(file, "", function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("The file was saved!");
  }); 
}

function splitUpArray(multiArray, columnsToSplit){

  var splitArray = [];

  for(var i = 0; i < multiArray.length; i++){
    var valueRow = "";
    for(var j = 0; j < columnsToSplit.length; j++){
       valueRow += multiArray[i][columnsToSplit[j]];
       if(!(j + 1 === columnsToSplit.length)) {
         valueRow += ',';
       }
    }
    splitArray.push(valueRow);
  }

  return splitArray;
}

function convertToMultiArray(array){

 if(array[0].constructor === Array){
  return array;
 }else{
  for (var i = 0; i < array.length; i++) {
    array[i] = array[i].split(',');
  }
  return array;
 }

  
}


function findIndex(multiArray, data, isDestArray, outputFileName){
  var headerArray = multiArray[0];
  var index = -1;

  if(isDestArray && outputFileName === './Clever/teachers.csv' 
    && data === 'Teacher_First_name'){data = 'First_name';
  }

  else if(isDestArray && outputFileName === './Clever/teachers.csv' 
    && data === 'Teacher_Last_name'){data = 'Last_name';
  }

  else if(isDestArray && outputFileName === './Clever/students.csv' 
    && data === 'Student_First_name'){data = 'First_name';
  }

  else if(isDestArray && outputFileName === './Clever/students.csv' 
    && data === 'Student_Last_name'){data = 'Last_name';
  }

  for(var i = 0; i < headerArray.length; i++){
      if(headerArray[i] === data){
        index = i;
    }
  }

  return index;
}

function writeToFile(file, contents){
  var stream = fs.createWriteStream(file);
    stream.once('open', function(fd) {
    contents.forEach(row => {
     stream.write(row.toString());
     stream.write('\n');
    });
    stream.end();
  });
}

function cleanArray(actual) {
  var newArray = [];
  for (var i = 0; i < actual.length; i++) {
    if (actual[i]) {
      newArray.push(actual[i]);
    }
  }
  return newArray;
}

//returns content to put into file
function setupFile(file, fileHeader){
  
  //if the file doesn't exist then create it
  if(!(fileExists(file))){createFile(file);}
  else{fs.truncateSync(file, 0, function(){console.log('done')});}
  //split up contents of combined file and manipulate the rows
  var contents = fs.readFileSync(file, 'utf-8');
  var rows = cleanArray(contents.split('\n'));

  if(rows[0] != fileHeader){
    //add header row
    rows.unshift(fileHeader);
  }

  return rows;
}

module.exports = runSplit;
