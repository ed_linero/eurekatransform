var fs = require('fs');

function runAppend(files, output, runFilter){
  files.forEach(file => {
    fs.readFile(file, function(err, data){

     if(err) throw err;

       fs.appendFile(output, data + '\n', function(err){
         if(err) throw err;

         if(file === files[files.length - 1]) runFilter();
      });
    });
  });

}

module.exports = runAppend;

