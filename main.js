var transform = require('./src/transform');
var filter = require('./src/filter');
var split = require('./src/split');
var sftpConnect = require('./src/sftp');
var load = sftpConnect.load;
var sshClient = require('sftp-promises');
var onFinished = require('on-finished');
var destroy = require('destroy');


const fs = require('fs');
const YAML = require('yamljs');
const _ = require('lodash');

const csvFolder = './CSV/';
const cleverFolder = './Clever/';

var csvs = [];
var combinedCSV = 'combined.csv';
var enrollmentsCSV = 'enrollments.csv';
var sectionsCSV = 'sections.csv';
var teachersCSV = 'teachers.csv';
var studentsCSV = 'students.csv';

var config = YAML.load('./config.yaml');

var sftpSource = new sshClient(config.SFTP.Source);
var sftpDestination = new sshClient(config.SFTP.Destination);



const headerRow = config.Headers.Combined.join(',');
const enrollmentsHeader = config.Headers.Enrollments.join(',');
const sectionsHeader = config.Headers.Sections.join(',');
const teachersHeader = config.Headers.Teachers.join(',');
const studentsHeader = config.Headers.Students.join(',');

const filterFromFile = config.Filters;

const enrollmentCols = config.Columns.Enrollments;
const sectionCols = config.Columns.Sections;
const teacherCols = config.Columns.Teachers;
const studentCols = config.Columns.Students;


sftpConnect(sftpSource, config.SFTP.Source, 'extract', csvFolder, config.SFTP.Source.path, main);

function main(){
	console.log('running main App');
	//remove content from combined file before applying stuff
	fs.truncateSync(csvFolder + combinedCSV, 0, function(){console.log('done')});

	transform(csvFolder, csvs, combinedCSV, function() {    
	    //apply filter function then run the callback to split the file into smaller files
		filter(csvFolder + combinedCSV, headerRow, filterFromFile, 
			function(filteredArray) {
				split(filteredArray, enrollmentCols, enrollmentsHeader, cleverFolder + enrollmentsCSV);
				split(filteredArray, studentCols, studentsHeader, cleverFolder + studentsCSV);
				split(filteredArray, teacherCols, teachersHeader, cleverFolder + teachersCSV);
				split(filteredArray, sectionCols, sectionsHeader, cleverFolder + sectionsCSV);
				sftpConnect(sftpDestination, config.SFTP.Destination, 'load', 
							cleverFolder, config.SFTP.Destination.path, 
							function(){console.log('done')});
			});
	});

}





