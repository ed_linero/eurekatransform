describe('convertToMultiArray', function(){
	it('it converts an array to a multidimensional array', function(){
		expect(allTrue([true,true,true])).to.be.true;
	});

	it('is false if there is a single false value', function(){
		expect(allTrue([true,false,true])).to.be.false;
	});
});
